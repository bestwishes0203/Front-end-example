<p align="center">
	<a href="https://bestwishes0203.github.io/blog/">
    <img src="http://cdn.mingsoft.net/global/images/logo-blue.png"  width="50" height="50">
</a>
</p>
<p align="center">
	<strong>BestWishes0203</strong>
</p>
<p align="center">
	<a target="_blank" href="https://search.maven.org/search?q=ms-mcms">
        <img src="https://img.shields.io/maven-central/v/net.mingsoft/ms-mcms.svg?label=Maven%20Central" alt="低代码平台，开源排名第一的java cms，免费开源的java cms, 免费开源Javacms,MCMS,开源Java CMS,Java网站建设工具,开源CMS,内容管理系统,CMS软件,网站建设工具,网站模板,响应式模板,自适应模板,多功能模板,网站插件,功能插件,扩展插件,定制插件,Java CMS定制"/>
	</a>
	<a target="_blank" href="hhttps://baike.baidu.com/item/MIT%E8%AE%B8%E5%8F%AF%E8%AF%81/6671281?fr=aladdin">
        <img src="https://img.shields.io/:license-MIT-blue.svg" alt="低代码平台，开源排名第一的java cms，免费开源的java cms, 免费开源Javacms,MCMS,开源Java CMS,Java网站建设工具,开源CMS,内容管理系统,CMS软件,网站建设工具,网站模板,响应式模板,自适应模板,多功能模板,网站插件,功能插件,扩展插件,定制插件,Java CMS定制" />
	</a>
	<a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html">
		<img src="https://img.shields.io/badge/JDK-8+-green.svg" alt="低代码平台，开源排名第一的java cms，免费开源的java cms, 免费开源Javacms,MCMS,开源Java CMS,Java网站建设工具,开源CMS,内容管理系统,CMS软件,网站建设工具,网站模板,响应式模板,自适应模板,多功能模板,网站插件,功能插件,扩展插件,定制插件,Java CMS定制"/>
	</a>
	<a target="_blank" href="https://gitee.com/mingSoft/MCMS/stargazers">
		<img src="https://gitee.com/mingSoft/MCMS/badge/star.svg?theme=dark" alt='低代码平台，开源排名第一的java cms，免费开源的java cms, 免费开源Javacms,MCMS,开源Java CMS,Java网站建设工具,开源CMS,内容管理系统,CMS软件,网站建设工具,网站模板,响应式模板,自适应模板,多功能模板,网站插件,功能插件,扩展插件,定制插件,Java CMS定制'/>
	</a>
	<a target="_blank" href='https://github.com/ming-soft/mcms'>
		<img src="https://img.shields.io/github/stars/ming-soft/mcms.svg?style=social" alt="低代码平台，开源排名第一的java cms，免费开源的java cms, 免费开源Javacms,MCMS,开源Java CMS,Java网站建设工具,开源CMS,内容管理系统,CMS软件,网站建设工具,网站模板,响应式模板,自适应模板,多功能模板,网站插件,功能插件,扩展插件,定制插件,Java CMS定制"/>
	</a>
</p>
<p align="center">
	<a href="https://gitee.com/bestwishes0203/Front-end-notes/" target="_blank">前端笔记</a> 
	<a href="https://gitee.com/bestwishes0203/Back-end-notes/" target="_blank">后端笔记</a> 
    <a href="https://gitee.com/bestwishes0203/intangible-cultural-heritage/"  target="_blank">中国非物质文化遗产前端</a> 
	<a href="https://gitee.com/bestwishes0203/Course-Manage"  target="_blank">大学选课系统</a>
	<a href="https://gitee.com/bestwishes0203/QQ-music" target="_blank">仿QQ音乐</a><br/>
	<a href="https://gitee.com/bestwishes0203/College-Administration"  target="_blank">教务管理系统</a><br/>
	<a href="https://bestwishes0203.github.io/blog/"  target="_blank">个人CSDN\个人博客</a>
</p>

-------------------------------------------------------------------------------

<p align="center">
	<strong>金风玉露一相逢，便胜却人间无数。</strong>
</p>

# QQ

[![加入QQ群](https://img.shields.io/badge/QQ-2109664977-blue.svg?logo=tencentqq&style=for-the-badge)](https://qm.qq.com/cgi-bin/qm/qr?k=FVvyoF1YwtFpCGLf--h42_AwtF6JOD9M&jump_from=webapi&authKey=EyNayGyX0U/6hJKtpBInh9FsCpeM8Vx2KqrmmCZyXe3f1PD5kboChBSIWkB6SOb5)

# 😍 前端学习笔记

## 😁 仓库介绍

本仓库是我的个人学习笔记，记录了从Web基础到Vue/React应用的一系列前端技术示例。这里不仅包含了H5、CSS、JS、Jquery、Echarts。还有Web实战的实用功能实现。创建这个仓库的初衷是为了巩固自己的知识，防止遗忘，同时也希望能为其他开发者提供一个互相学习和借鉴的平台。我会持续更新这个仓库，与大家分享更多的学习心得和技术经验。

--- 

## 🗂️ 笔记架构

- **React**:
    - **富文本编辑器**: 使用React实现的富文本编辑器，支持丰富的文本格式和样式编辑功能。
    - **React (富文本编辑器)**: 基于React的富文本编辑器，提供所见即所得的编辑体验。
    - **其他笔记**: 正在学习更新...

- **Vue**:
    - **Vue-am-editor**: 基于Vue的富文本编辑器，支持多种编辑功能和插件扩展。
    - **Vue-echarts-maps**: 集成ECharts的Vue组件，用于创建交互式的图表地图。
    - **Vue-echarts**: 使用Vue封装的ECharts图表库，简化图表的创建和操作。
    - **Vue-md-editor**: 基于Vue的Markdown编辑器，支持Markdown语法的编写和预览。
    - **Vue-mockjs**: 提供虚拟接口服务，用于前端开发中的API模拟。
    - **Vue-vuex**: Vue的全局状态管理工具，用于构建复杂的应用状态管理。
    - **Vue-wangeditor**: 基于Vue的富文本编辑器，提供丰富的编辑功能和良好的用户体验。
    - **其他笔记**: 正在学习更新...

--- 

## ✌️使用教程

1. **克隆仓库**:

```shell
git clone https://gitee.com/your-username/frontend-examples
```

2. **安装依赖**:

```shell
cd frontend-examples
npm install
```

3. **运行示例**:

```shell
npm run dev
```

## 😊 使用说明

- **查看示例**: 在`src`目录下，你可以找到各个示例项目的入口文件。
- **学习文档**: 每个示例项目都包含详细的使用说明和文档，帮助你快速上手。

---

## 🚀 获取笔记

- **后端学习笔记**：[https://gitee.com/bestwishes0203/Front-end-notes](https://gitee.com/bestwishes0203/Front-end-notes)
- **前端学习笔记**：[https://gitee.com/bestwishes0203/Back-end-notes](https://gitee.com/bestwishes0203/Back-end-notes)

---

## 📌 学习交流

如果您对我们的项目感兴趣，或者有任何技术问题想要探讨，欢迎通过以下方式与我联系。我非常期待与您交流，共同学习，共同进步！🌊💡🤖

- **邮箱**：[2109664977@qq.com](mailto:2109664977@qq.com) 📧
- **Gitee**：[https://gitee.com/bestwishes0203](https://gitee.com/bestwishes0203) 🐱
- **GitHub**：[https://github.com/bestwishes0203](https://github.com/bestwishes0203) 🐙
- **CSDN**：[https://blog.csdn.net/interest_ing_/](https://blog.csdn.net/interest_ing_/) 📖
- **个人博客**：[http://bestwishes0203.github.io/blog/](http://bestwishes0203.github.io/blog/) 🏠

---

## 🎉 结语

感谢你的访问，期待与你在技术的道路上相遇！👋🌟🚀

---



