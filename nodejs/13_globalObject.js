/**
 * __filename
__filename 表示当前正在执行的脚本的文件名。它将输出文件所在位置的绝对路径，且和命令行参数所指定的文件名不一定相同。 如果在模块中，返回的值是模块文件的路径。

实例
创建文件 main.js ，代码如下所示：

// 输出全局变量 __filename 的值
console.log( __filename );
执行 main.js 文件，代码如下所示:

$ node main.js
/web/com/runoob/nodejs/main.js
__dirname
__dirname 表示当前执行脚本所在的目录。

实例
创建文件 main.js ，代码如下所示：

// 输出全局变量 __dirname 的值
console.log( __dirname );
执行 main.js 文件，代码如下所示:

$ node main.js
/web/com/runoob/nodejs
setTimeout(cb, ms)
setTimeout(cb, ms) 全局函数在指定的毫秒(ms)数后执行指定函数(cb)。：setTimeout() 只执行一次指定函数。

返回一个代表定时器的句柄值。

实例
创建文件 main.js ，代码如下所示：

function printHello(){
   console.log( "Hello, World!");
}
// 两秒后执行以上函数
setTimeout(printHello, 2000);
执行 main.js 文件，代码如下所示:

$ node main.js
Hello, World!
clearTimeout(t)
clearTimeout( t ) 全局函数用于停止一个之前通过 setTimeout() 创建的定时器。 参数 t 是通过 setTimeout() 函数创建的定时器。

实例
创建文件 main.js ，代码如下所示：

function printHello(){
   console.log( "Hello, World!");
}
// 两秒后执行以上函数
var t = setTimeout(printHello, 2000);

// 清除定时器
clearTimeout(t);
执行 main.js 文件，代码如下所示:

$ node main.js
setInterval(cb, ms)
setInterval(cb, ms) 全局函数在指定的毫秒(ms)数后执行指定函数(cb)。

返回一个代表定时器的句柄值。可以使用 clearInterval(t) 函数来清除定时器。

setInterval() 方法会不停地调用函数，直到 clearInterval() 被调用或窗口被关闭。

实例
创建文件 main.js ，代码如下所示：

function printHello(){
   console.log( "Hello, World!");
}
// 两秒后执行以上函数
setInterval(printHello, 2000);
执行 main.js 文件，代码如下所示:

$ node main.jsHello, World! Hello, World! Hello, World! Hello, World! Hello, World! ……
以上程序每隔两秒就会输出一次"Hello, World!"，且会永久执行下去，直到你按下 ctrl + c 按钮。
 */
