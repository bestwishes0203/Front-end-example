// Node.js Net 模块提供了一些用于底层的网络通信的小工具，包含了创建服务器/客户端的方法，我们可以通过以下方式引入该模块：

// 创建 server.js 文件，代码如下所示：

var net = require("net");
var server = net.createServer(function (connection) {
  console.log("client connected");
  connection.on("end", function () {
    console.log("客户端关闭连接");
  });
  connection.write("Hello World!\r\n");
  connection.pipe(connection);
});
server.listen(8080, function () {
  console.log("server is listening");
});

// 新开一个窗口，创建 client.js 文件，代码如下所示：

var net = require("net");
var client = net.connect({ port: 8080 }, function () {
  console.log("连接到服务器！");
});
client.on("data", function (data) {
  console.log(data.toString());
  client.end();
});
client.on("end", function () {
  console.log("断开与服务器的连接");
});
