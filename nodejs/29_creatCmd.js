const { spawn } = require('child_process');

// 使用spawn调起一个新的cmd窗口
const cmd = spawn('cmd.exe', [], {
  detached: true, // 使子进程在后台运行
  stdio: 'inherit' // 继承父进程的输入输出流
});

// 当cmd窗口关闭时，打印退出码
cmd.on('close', (code) => {
  console.log(`命令提示符已关闭，退出码：${code}`);
});
