var express = require('express');
var app = express();

app.use('/public', express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/templates/" + "index.html");
})
app.get('/user', function (req, res) {
    res.sendFile(__dirname + "/templates/" + "user.html");
})
app.get('/books', function (req, res) {
    res.sendFile(__dirname + "/templates/" + "books.html");
})
app.get('/book', function (req, res) {
    res.sendFile(__dirname + "/templates/" + "book.html");
})
app.get('/borrow', function (req, res) {
    res.sendFile(__dirname + "/templates/" + "borrow.html");
})
app.get('/mine', function (req, res) {
    res.sendFile(__dirname + "/templates/" + "mine.html");
})
app.get('/*', function (req, res) {
    res.sendFile(__dirname + "/templates/" + "404.html");
})

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    if (host === `::`) {
        host = "127.0.0.1"
    }
    console.log("应用实例，访问地址为 http://%s:%s", host, port)
})