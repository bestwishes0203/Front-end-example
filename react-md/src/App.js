// import React, { useState } from 'react';
// import { marked } from 'marked'; // 修改导入方式

// function App() {
//   const [markdown, setMarkdown] = useState(`

//   数学公式:

//   Markdown中的数学公式可以使用 LaTeX 语法来编写:

//   行内公式: $E=mc^2$ 表示质能等价。

//   块级公式: 

//   $$
//   \begin{align*}

//   e^x & = 1 + x + \frac{x^2}{2!} + \frac{x^3}{3!} + \frac{x^4}{4!} + \cdots \\

//     & = \sum_{n=0}^{\infty} \frac{x^n}{n!}

//   \end{align*}
//   $$

//   这个公式展示了自然指数函数的泰勒级数展开。




//   **方法概述 (Method Overview)**

//   本研究旨在通过描述性统计方法对实验数据进行分析，以揭示数据的基本特征和分布趋势。描述性统计包括度量集中趋势的均值、众数和中位数，以及度量分散程度的方差、标准差和四分位距等。在本研究中，我们使用了均值（Mean）、方差（Variance）、标准差（Standard Deviation）以及四分位间距（Interquartile Range, IQR）等统计量。其计算公式如下：

//   \[
//   \text{Mean} ( \bar{x} ) = \frac{1}{n} \sum_{i=1}^{n} x_i
//   \]  
//   \[
//   \text{Variance} ( \sigma^2 ) = \frac{1}{n-1} \sum_{i=1}^{n} (x_i - \bar{x})^2
//   \]  
//   \[
//   \text{Standard Deviation} ( \sigma ) = \sqrt{\sigma^2}
//   \]  
//   \[
//   \text{Interquartile Range} = Q_3 - Q_1
//   \]  


//   $$
//   \text{Mean} ( \bar{x} ) = \frac{1}{n} \sum_{i=1}^{n} x_i
//   $$

//   $$
//   \text{Variance} ( \sigma^2 ) = \frac{1}{n-1} \sum_{i=1}^{n} (x_i - \bar{x})^2
//   $$

//   $$
//   \text{Standard Deviation} ( \sigma ) = \sqrt{\sigma^2}
//   $$

//   $$
//   \text{Interquartile Range} = Q_3 - Q_1
//   $$

//   其中，\( x_i \) 表示观测值，\( n \) 是观测值的总数量，\( Q_3 \) 和 \( Q_1 \) 分别表示第三和第一四分位数。

//   **统计说明**

//   所有统计分析均使用 R 版本 4.2.3 和 Python 版本 3.11.4 完成。本研究的统计分析在极智分析平台 (https://www.xsmartanalysis.com/) 上进行，以确保结果的准确性和可重现性。

//   **结果（Results）**

//   **3.1 结果描述**

//   本研究分析的数据来源于一个大型的临床试验，包含数千个数据点。变量包括患者的年龄、体重、血压等。数据分布表现出某些偏态特征，但总体符合正态分布。样本量足够大，能够对描述性统计量进行稳健的估计。

//   **3.2 结果图表解读**

//   **Table 1: 关键描述性统计量的数据汇总**

//   | 变量 | 均值 | 标准差 | 中位数 | 四分位间距 |
//   |------|------|--------|--------|------------|
//   | 年龄 | 45.6 | 8.4    | 46     | 12         |
//   | 体重 | 75.3 | 14.2   | 74.5   | 20         |
//   | 血压 | 120.5 | 15.6  | 118    | 22         |

//   *表注：Table 1：总结各个变量的描述性统计量，展示了数据的中心和分散趋势。*

//   Table 1 展示了参与者年龄、体重和血压的基本统计描述。年龄的均值为 45.6 岁，标准差为 8.4，显示出中等程度的分散。体重的均值为 75.3 公斤，均值附近的标准差表示正常波动。血压的均值为 120.5 mmHg，标准差为 15.6。

//   年龄和体重的中位数与其均值接近，表明这些数据大致对称。四分位间距提供了数据中心 50% 的范围，有助于理解数据的变异性。

//   **总结 (Summarize)**

//   本研究通过描述性统计方法有效地描述了临床数据的基本特征。均值、中位数和标准差等统计量为我们提供了数据中心趋势和离散程度的综合视图。通过四分位距，我们捕获了数据的变异性区间。方法的应用揭示了不同变量在群体中的分布，为进一步的统计分析及模型建立提供了有力支持。

//   **参考文献**

//   1. Field, A. P. (2018). *Discovering Statistics Using R*. London: Sage Publications. 
//   2. Montgomery, D. C., & Runger, G. C. (2014). *Applied Statistics and Probability for Engineers*. John Wiley & Sons.
//   3. Hair, J. F., Black, W. C., Babin, B. J., & Anderson, R. E. (2019). *Multivariate Data Analysis*. Cengage Learning EMEA.

// `);

//   const html = marked(markdown);

//   return (
//     <div className="App">
//       <h1>Markdown Renderer</h1>
//       <textarea
//         value={markdown}
//         onChange={(e) => setMarkdown(e.target.value)}
//         rows="10"
//         cols="50"
//       />
//       <h2>Rendered HTML:</h2>
//       <div dangerouslySetInnerHTML={{ __html: html }} />
//     </div>
//   );
// }

// export default App;





// **************************************************************************************************

// import React, { useState } from 'react';
// import Markdown from './components/Markdown';

// function App() {
//   const [content, setContent] = useState(`


//   **方法概述 (Method Overview)**

//   本研究旨在通过描述性统计方法对实验数据进行分析，以揭示数据的基本特征和分布趋势。描述性统计包括度量集中趋势的均值、众数和中位数，以及度量分散程度的方差、标准差和四分位距等。在本研究中，我们使用了均值（Mean）、方差（Variance）、标准差（Standard Deviation）以及四分位间距（Interquartile Range, IQR）等统计量。其计算公式如下：

//   \[
//   \text{Mean} ( \bar{x} ) = \frac{1}{n} \sum_{i=1}^{n} x_i
//   \]  
//   \[
//   \text{Variance} ( \sigma^2 ) = \frac{1}{n-1} \sum_{i=1}^{n} (x_i - \bar{x})^2
//   \]  
//   \[
//   \text{Standard Deviation} ( \sigma ) = \sqrt{\sigma^2}
//   \]  
//   \[
//   \text{Interquartile Range} = Q_3 - Q_1
//   \]  


//   $$
//   \text{Mean} ( \bar{x} ) = \frac{1}{n} \sum_{i=1}^{n} x_i
//   $$

//   $$
//   \text{Variance} ( \sigma^2 ) = \frac{1}{n-1} \sum_{i=1}^{n} (x_i - \bar{x})^2
//   $$

//   $$
//   \text{Standard Deviation} ( \sigma ) = \sqrt{\sigma^2}
//   $$

//   $$
//   \text{Interquartile Range} = Q_3 - Q_1
//   $$


// `);

//   const handleInput = (e) => {
//     setContent(e.target.value);
//   };

//   return (
//     <div>
//       <textarea
//         value={content}
//         onInput={handleInput}
//         rows={8}
//       />
//       <Markdown content={content} />
//     </div>
//   );
// }

// export default App;








// import React from 'react';
// import Markdown from './components/Markdown';

// function App() {
//   const markdownContent = `


//   # LaTeX 公式示例

//   以下是几个统计学中的公式：
  
//   $$
//   \text{Mean} ( \\bar{x} ) = \\frac{1}{n} \\sum_{i=1}^{n} x_i
//   $$
  
//   $$
//   \text{Variance} ( \\sigma^2 ) = \\frac{1}{n-1} \\sum_{i=1}^{n} (x_i - \\bar{x})^2
//   $$
  
//   $$
//   \text{Standard Deviation} ( \\sigma ) = \\sqrt{\\sigma^2}
//   $$
  
//   $$
//   \text{Interquartile Range} = Q_3 - Q_1
//   $$
  

  
//   1.1 中心趋势度量： 

// $$
// \text{均值} (\bar{x}) = \frac{1}{N}\sum_{i=1}^{N} x_i
// $$

// $$
// \text{中位数} = \begin{cases} 
// x_{(n+1)/2}, & \text{当 }n \text{是奇数} \\ 
// \frac{x_{(n/2)} + x_{(n/2 + 1)}}{2}, & \text{当 }n \text{是偶数} 
// \end{cases}
// $$

//   1.2 变异性度量：

// $$
// \text{方差} (s^2) = \frac{1}{N-1}\sum_{i=1}^{N} (x_i - \bar{x})^2
// $$

// $$
// \text{标准差} (s) = \sqrt{s^2}
// $$

//   1.3 分布形状指标：  

//   $$
//   \text{偏度} = \frac{1}{N}\sum_{i=1}^{N} \left(\frac{x_i - \bar{x}}{s}\right)^3
//   $$
  
//   $$
//   \text{峰度} = \frac{1}{N}\sum_{i=1}^{N} \left(\frac{x_i - \bar{x}}{s}\right)^4 - 3
//   $$

//   `;

//   return (
//     <div className="App">
//       <Markdown content={markdownContent} />
//     </div>
//   );
// }

// export default App;












import React from 'react';
import ReactMarkdown from 'react-markdown';
import remarkMath from 'remark-math';
import rehypeKatex from 'rehype-katex';
import rehypeHighlight from 'rehype-highlight';
import remarkGfm from 'remark-gfm';
import remarkBreaks from 'remark-breaks';
import 'katex/dist/katex.min.css';

function Markdown({ content }) {
  return (
    <ReactMarkdown
      remarkPlugins={[remarkMath, remarkGfm, remarkBreaks]}
      rehypePlugins={[rehypeKatex, rehypeHighlight]}
    >
      {content}
    </ReactMarkdown>
  );
}

function App() {
  const markdownContent = `

  $$
  \begin{align*}
  
  e^x & = 1 + x + \frac{x^2}{2!} + \frac{x^3}{3!} + \frac{x^4}{4!} + \cdots \\
  
    & = \sum_{n=0}^{\infty} \frac{x^n}{n!}
  
  \end{align*}
  $$
  
  
  $$
  \text{Mean} ( \bar{x} ) = \frac{1}{n} \sum_{i=1}^{n} x_i
  $$
  
  $$
  \text{Variance} ( \sigma^2 ) = \frac{1}{n-1} \sum_{i=1}^{n} (x_i - \bar{x})^2
  $$
  
  $$
  \text{Standard Deviation} ( \sigma ) = \sqrt{\sigma^2}
  $$
  
  $$
  \text{Interquartile Range} = Q_3 - Q_1
  $$
  
  
  ****************************************************************************************
  
  
      1.1 中心趋势度量： 
  
  $$
  \text{均值} (\bar{x}) = \frac{1}{N}\sum_{i=1}^{N} x_i
  $$
  
  $$
    \text{中位数} = \begin{cases} 
    x_{(n+1)/2}, & \text{当 }n \text{是奇数} \\ 
    \frac{x_{(n/2)} + x_{(n/2 + 1)}}{2}, & \text{当 }n \text{是偶数} 
    \end{cases}
  $$
  
    1.2 变异性度量：
  
  $$
  \text{方差} (s^2) = \frac{1}{N-1}\sum_{i=1}^{N} (x_i - \bar{x})^2
  $$
  
  $$
  \text{标准差} (s) = \sqrt{s^2}
  $$
  
    1.3 分布形状指标：  
  
  $$
  \text{偏度} = \frac{1}{N}\sum_{i=1}^{N} \left(\frac{x_i - \bar{x}}{s}\right)^3
  $$
  
  $$
    \text{峰度} = \frac{1}{N}\sum_{i=1}^{N} \left(\frac{x_i - \bar{x}}{s}\right)^4 - 3
  $$
  
  ****************************************************************************************
  
  
    1.1 中心趋势度量： 
  $$
  \\text{均值} (\\bar{x}) = \\frac{1}{N}\\sum_{i=1}^{N} x_i
  $$
  
  $$
  \\text{中位数} = \\begin{cases} 
  x_{(n+1)/2}, & \\text{当 }n \\text{是奇数} \\\\ 
  \\frac{x_{(n/2)} + x_{(n/2 + 1)}}{2}, & \\text{当 }n \\text{是偶数} 
  \\end{cases}
  $$
  
    1.2 变异性度量：
  
  $$
  \\text{方差} (s^2) = \\frac{1}{N-1}\\sum_{i=1}^{N} (x_i - \\bar{x})^2
  $$
  
  $$
  \\text{标准差} (s) = \\sqrt{s^2}
  $$
  
    1.3 分布形状指标： 
  
  $$
  \\text{偏度} = \\frac{1}{N}\\sum_{i=1}^{N} \\left(\\frac{x_i - \\bar{x}}{s}\\right)^3
  $$
  
  $$
  \\text{峰度} = \\frac{1}{N}\\sum_{i=1}^{N} \\left(\\frac{x_i - \\bar{x}}{s}\\right)^4 - 3
  $$
  
  ****************************************************************************************
  
  
  
  1.1 中心趋势度量：  
  
  \[
  \text{均值} (\bar{x}) = \frac{1}{N}\sum_{i=1}^{N} x_i
  \]
  \[
  \text{中位数} = \begin{cases} 
  x_{(n+1)/2}, & \text{当 }n \text{是奇数} \\ 
  \frac{x_{(n/2)} + x_{(n/2 + 1)}}{2}, & \text{当 }n \text{是偶数} 
  \end{cases}
  \]
  1.2 变异性度量：  
  \[
  \text{方差} (s^2) = \frac{1}{N-1}\sum_{i=1}^{N} (x_i - \bar{x})^2
  \]
  \[
  \text{标准差} (s) = \sqrt{s^2}
  \]
  1.3 分布形状指标：  
  \[
  \text{偏度} = \frac{1}{N}\sum_{i=1}^{N} \left(\frac{x_i - \bar{x}}{s}\right)^3
  \]
  \[
  \text{峰度} = \frac{1}{N}\sum_{i=1}^{N} \left(\frac{x_i - \bar{x}}{s}\right)^4 - 3
  \]
  



`;

  return (
    <div className="App">
      <Markdown content={markdownContent} />
    </div>
  );
}

export default App;