// import React from 'react';
// import ReactMarkdown from 'react-markdown';
// import rehypeHighlight from 'rehype-highlight';
// import 'highlight.js/styles/atom-one-dark.css';

// const Markdown = ({ content }) => {
//     return (
//         <ReactMarkdown
//             rehypePlugins={[rehypeHighlight]}
//         >
//             {content}
//         </ReactMarkdown>
//     );
// };

// export default Markdown;








import "katex/dist/katex.min.css";
import React from 'react';
import ReactMarkdown from 'react-markdown';
import RemarkMath from 'remark-math';
import RemarkGfm from 'remark-gfm';
import RemarkBreaks from 'remark-breaks';
import RehypeKatex from 'rehype-katex';
import RehypeHighlight from 'rehype-highlight';

function Markdown({ content }) {
    return (
        <ReactMarkdown
            remarkPlugins={[RemarkMath, RemarkGfm, RemarkBreaks]}
            rehypePlugins={[RehypeKatex, RehypeHighlight]}
        >
            {content}
        </ReactMarkdown>
    );
}

export default Markdown;