import Vue from 'vue'
import App from './App.vue'

// import fabric from 'fabric';
// Vue.use(fabric);

// import 'vue-fabric/dist/vue-fabric.min.css';
import { Fabric } from 'vue-fabric';
Vue.use(Fabric);

Vue.config.productionTip = false

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
