```javascript
// main.js
import Vue from 'vue';
import App from './App.vue';

// 创建一个事件总线
const EventBus = new Vue();

// 将其添加到Vue的原型上，以便在任何组件中都能访问
Vue.prototype.$bus = EventBus;

new Vue({
  render: h => h(App),
}).$mount('#app');
```


