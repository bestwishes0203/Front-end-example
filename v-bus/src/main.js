import Vue from 'vue'
import App from './App.vue'
import { EventBus } from './utils/event-bus.js';

 // 将事件总线添加到Vue原型，使其可以在任何组件中通过this.$bus访问
Vue.prototype.$bus = EventBus;
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
