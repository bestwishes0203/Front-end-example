const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: "http://127.0.0.1:8000",
    client: {
      overlay: false,//解决全屏报错
    },
  },
})
