import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import Katex from 'vue-katex-auto-render';
import 'katex/dist/katex.min.css';
Vue.directive('katex', Katex);

// Vue.directive('katex', Katex({
//   delimiters: [
//     { left: '$$', right: '$$', display: true },
//     { left: '$', right: '$', display: false },
//     { left: '\\(', right: '\\)', display: false },
//     { left: '\\[', right: '\\]', display: true }
//   ]
// }));

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
