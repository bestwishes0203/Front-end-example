import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import VueKatex from 'vue-katex';
import 'katex/dist/katex.min.css'; // 导入 KaTeX 的样式文件
Vue.use(VueKatex);

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
