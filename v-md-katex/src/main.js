import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// "vue-katex": "^0.5.0"
// import VueKatex from "vue-katex";
import "katex/dist/katex.min.css";
// Vue.use(VueKatex, {
//   globalOptions: {
//     //定义好界定符，好让它能够找到渲染的latex公式块
//     delimiters: [
//       { left: "$$", right: "$$", display: true },
//       { left: "$", right: "$", display: false },
//       { left: "\\(", right: "\\)", display: false },
//       { left: "\\[", right: "\\]", display: true },
//     ],
//     throwOnError:false
//   },
// });

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
