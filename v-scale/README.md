在Vue 2项目中使用`vue2-scale-box`进行屏幕缩放适配，可以通过以下步骤实现：

1. **安装`vue2-scale-box`**：
   首先，你需要在你的Vue 2项目中安装`vue2-scale-box`。可以通过npm或yarn来安装：
   
   ```shell
   npm install vue2-scale-box
   # 或者
   yarn add vue2-scale-box
   ```

2. **在`App.vue`中使用`ScaleBox`组件**：
   在你的`App.vue`文件中，引入并注册`ScaleBox`组件，并使用它包裹你的应用视图（通常是`<router-view />`）：
   ```html
   <template>
     <div>
         <scale-box :width="1920" :height="1080" bgc="transparent" :delay="100">
             <router-view />
         </scale-box>
     </div>
   </template>
   <script>
   import ScaleBox from "vue2-scale-box";
   export default {
       components: { ScaleBox },
   };
   </script>
   ```
   
3. **`ScaleBox`组件的属性**：
   - `width`：设计稿的宽度，默认为`1920`。
   - `height`：设计稿的高度，默认为`1080`。
   - `bgc`：背景颜色，默认为`"transparent"`。
   - `delay`：自适应缩放防抖延迟时间（ms），默认为`100`。
   

通过以上步骤，你就可以在Vue 2项目中使用`vue2-scale-box`来实现屏幕缩放适配了。这个组件会自动根据设置的宽高比例进行缩放，并且包含了屏幕缩放防抖优化，使得在不同分辨率的屏幕上都能保持良好的视觉效果。

