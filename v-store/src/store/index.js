// import Vue from 'vue';
// import Vuex from 'vuex';
// 
// Vue.use(Vuex);
// 
// export default new Vuex.Store({
//   state: {
//     count: 0 // 初始计数为0
//   },
//   mutations: {
//     increment(state) {
//       state.count++; // 增加计数
//     }
//   },
//   actions: {
//     increment({ commit }) {
//       commit('increment'); // 提交一个 mutation 来改变状态
//     }
//   },
//   getters: {
//     count: state => state.count // 计算属性，返回当前计数
//   }
// });



import Vue from 'vue';
import Vuex from 'vuex';
import moduleA from './modules/moduleA';
import moduleB from './modules/moduleB';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    a: moduleA,
    b: moduleB
  }
});