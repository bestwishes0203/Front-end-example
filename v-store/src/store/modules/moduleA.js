// src/store/modules/moduleA.js
export default {
    namespaced: true, // 重要：标记为命名空间模块
    state: {
        counterA: 0
    },
    mutations: {
        incrementA(state) {
            state.counterA++;
        }
    },
    actions: {
        incrementA({ commit }) {
            commit('incrementA');
        }
    },
    getters: {
        counterA: state => state.counterA
    }
};