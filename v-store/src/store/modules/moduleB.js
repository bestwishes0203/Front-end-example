// src/store/modules/moduleB.js
export default {
    namespaced: true,
    state: {
        counterB: 0
    },
    mutations: {
        incrementB(state) {
            state.counterB++;
        }
    },
    actions: {
        incrementB({ commit }) {
            commit('incrementB');
        }
    },
    getters: {
        counterB: state => state.counterB
    }
};