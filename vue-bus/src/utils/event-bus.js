// event-bus.js
import { reactive, readonly } from 'vue';

// 创建一个响应式的事件总线对象
const state = reactive(new Map());

// 提供一个函数来发射事件
function emit(event, payload) {
    (state.get(event) || []).forEach((callback) => callback(payload));
}

// 提供一个函数来监听事件
function on(event, callback) {
    if (!state.has(event)) {
        state.set(event, []);
    }
    state.get(event).push(callback);
    return () => off(event, callback);
}

// 提供一个函数来移除事件监听器
function off(event, callback) {
    const callbacks = state.get(event);
    if (callbacks) {
        callbacks.splice(callbacks.indexOf(callback), 1);
    }
}

// 提供一个函数来清除所有事件监听器
function clear() {
    state.clear();
}

export const EventBus = {
    emit,
    on,
    off,
    clear,
    readonly: readonly(state),
};