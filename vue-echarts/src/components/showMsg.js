// import MessageBox from "@/components/MessageBox.vue";
import { createApp } from "vue";
import { styled } from "@styils/vue";

const DivModal = styled("div", {
  position: "fixed",
  width: "100%",
  height: "100%",
  top: "0",
  left: "0",
  background: "rgba(0,0,0,.4)",
});

const DivBox = styled("div", {
  position: "fixed",
  width: "300px",
  height: "100px",
  top: "40%",
  left: "calc(50% - 150px)",
  background: "white",
  borderRadius: "10px",
  border: "2px solid #707070",
  color: "#000",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
});

const DivButton = styled("el-button", {
  cursor: "pointer",
  borderRadius: "5px",
  padding: "5px 10px",
  background: "#409eff",
  color: "white",
  fontSize: "14px",
});

const MessageBox = {
  props: {
    msg: {
      type: String,
      required: true,
    },
  },
  render(ctx) {
    const { $props, $emit } = ctx;
    return (
      <DivModal>
        <DivBox>
          <h4>{$props.msg}</h4>
          <DivButton click={$emit("onClick")}>确认</DivButton>
        </DivBox>
      </DivModal>
    );
  },
};

function showMsg(msg, clickHandle) {
  const div = document.createElement("div");
  document.body.appendChild(div);

  const app = createApp(MessageBox, {
    msg,
    onClick() {
      clickHandle &
        clickHandle(() => {
          app.unmount(div);
          div.remove();
        });
    },
  });
  app.mount(div);
}

export default showMsg;
