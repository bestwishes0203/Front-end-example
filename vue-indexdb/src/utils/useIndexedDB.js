// useIndexedDB.js
import { ref } from 'vue';

let db;

const openDB = (dbName, storeName, version = 1) => {
    return new Promise((resolve, reject) => {
        const request = indexedDB.open(dbName, version);
        request.onsuccess = (event) => {
            db = event.target.result;
            resolve(db);
        };
        request.onerror = (event) => {
            reject(event.target.error);
        };
        request.onupgradeneeded = (event) => {
            db = event.target.result;
            if (!db.objectStoreNames.contains(storeName)) {
                db.createObjectStore(storeName, { keyPath: "id" });
            }
        };
    });
};

const closeDB = () => {
    if (db) {
        db.close();
    }
};

const addData = (storeName, data) => {
    return new Promise((resolve, reject) => {
        const transaction = db.transaction([storeName], "readwrite");
        const store = transaction.objectStore(storeName);
        const request = store.add(data);
        request.onsuccess = () => {
            resolve(request.result);
        };
        request.onerror = () => {
            reject(request.error);
        };
    });
};

const getData = (storeName, key) => {
    return new Promise((resolve, reject) => {
        const transaction = db.transaction([storeName], "readonly");
        const store = transaction.objectStore(storeName);
        const request = store.get(key);
        request.onsuccess = () => {
            resolve(request.result);
        };
        request.onerror = () => {
            reject(request.error);
        };
    });
};

const updateData = (storeName, key, data) => {
    return new Promise((resolve, reject) => {
        const transaction = db.transaction([storeName], "readwrite");
        const store = transaction.objectStore(storeName);
        const request = store.get(key);
        request.onsuccess = () => {
            const oldData = request.result;
            if (oldData) {
                Object.assign(oldData, data);
                const updateRequest = store.put(oldData);
                updateRequest.onsuccess = () => {
                    resolve(updateRequest.result);
                };
                updateRequest.onerror = () => {
                    reject(updateRequest.error);
                };
            }
        };
        request.onerror = () => {
            reject(request.error);
        };
    });
};

const deleteData = (storeName, key) => {
    return new Promise((resolve, reject) => {
        const transaction = db.transaction([storeName], "readwrite");
        const store = transaction.objectStore(storeName);
        const request = store.delete(key);
        request.onsuccess = () => {
            resolve(request.result);
        };
        request.onerror = () => {
            reject(request.error);
        };
    });
};

const getAllData = (storeName) => {
    return new Promise((resolve, reject) => {
        const transaction = db.transaction([storeName], "readonly");
        const store = transaction.objectStore(storeName);
        const request = store.getAll();
        request.onsuccess = () => {
            resolve(request.result);
        };
        request.onerror = () => {
            reject(request.error);
        };
    });
};

export default function useIndexedDB() {
    return {
        openDB,
        closeDB,
        addData,
        getData,
        updateData,
        deleteData,
        getAllData,
    };
}