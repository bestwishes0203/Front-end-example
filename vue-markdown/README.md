[官网](https://code-farmer-i.github.io/vue-markdown-editor/zh/#%E8%BD%BB%E9%87%8F%E7%89%88%E7%BC%96%E8%BE%91%E5%99%A8)

```shell

# 使用 npm
npm i @kangc/v-md-editor -S

# 使用yarn
yarn add @kangc/v-md-editor

```


```js
//vue.2
import Vue from 'vue';
import VueMarkdownEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';

import Prism from 'prismjs';

VueMarkdownEditor.use(vuepressTheme, {
    Prism,
});

Vue.use(VueMarkdownEditor);
```

```js
//vue.3
import { createApp } from 'vue';
import VueMarkdownEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';

import Prism from 'prismjs';

VueMarkdownEditor.use(vuepressTheme, {
    Prism,
});

const app = createApp(/*...*/);

app.use(VueMarkdownEditor);

```


```html

<template>
  <v-md-editor v-model="text" height="400px"></v-md-editor>
</template>

<script>
export default {
  data() {
    return {
      text: '',
    };
  },
};
</script>

```

text
类型：String
需要解析预览的 markdown 字符串。

注意

只有预览组件支持该属性。

#v-model
类型：String
支持双向绑定。

注意

只有编辑组件支持该属性。

#mode
类型：String
默认值: editable
模式。可选值：edit(纯编辑模式) editable(编辑与预览模式) preview(纯预览模式)。

#height
类型：String
默认值： ''
正常模式下编辑器的高度。

#tab-size
类型：Number
默认值： 2
编辑和预览时制表符的长度，编辑器和预览组件都支持该属性。

#toc-nav-position-right
类型：Boolean
默认值： false
目录导航是否在右侧。

#default-show-toc
类型：Boolean
默认值： false
是否默认显示目录导航。

#placeholder
类型：String
#autofocus
类型：Boolean
默认值： false
编辑器加载完是否自动聚焦。

#default-fullscreen
type：Boolean
default： false
是否默认开启全屏。

#include-level
类型: Array
默认值: [2, 3]
目录导航生成时包含的标题。默认包含 2 级、3 级标题。

#left-toolbar
类型：String
默认值： undo redo clear | h bold italic strikethrough quote | ul ol table hr | link image code | save
名称	说明
undo	撤销
redo	重做
clear	清空
h	标题
bold	粗体
italic	斜体
strikethrough	中划线
quote	引用
ul	无序列表
ol	有序列表
table	表格
hr	分割线
link	链接
image	插入图片
code	代码块
save	保存，点击后触发save事件
左侧工具栏

#right-toolbar
类型：String
默认值： preview toc sync-scroll fullscreen
右侧工具栏

名称	说明
preview	预览
toc	目录导航
sync-scroll	同步滚动
fullscreen	全屏


