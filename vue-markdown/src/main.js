import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'


//1."@kangc/v-md-editor": "^2.3.18",
//@ts-ignore
import VueMarkdownEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
//@ts-ignore
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';
//@ts-ignore
import Prism from 'prismjs';
VueMarkdownEditor.use(vuepressTheme, {
    Prism,
});

createApp(App).use(VueMarkdownEditor).mount('#app')
