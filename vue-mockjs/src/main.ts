import { createApp } from 'vue'
import App from './App.vue'
import '@/mock'

createApp(App).mount('#app')
