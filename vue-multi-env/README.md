# Vite多环境开发和打包

在使用 Vite 进行 Vue 开发时，实现多环境开发和打包主要涉及到环境变量的配置和不同的构建目标设置。以下是如何使用 Vite 实现多环境开发和打包的步骤：

### 1. 安装依赖

```shell
npm install --save-dev cross-env
```

### 2. 配置环境变量

在项目根目录下创建 `.env` 文件，分别为不同环境创建不同的环境变量配置。例如：

- `.env`：默认环境
- `.env.development`：开发环境
- `.env.production`：生产环境

在这些文件中，你可以设置不同的环境变量，例如 API 端点或其他服务的配置。

```plaintext
# .env
VITE_API_URL=https://api.example.com

# .env.development
VITE_API_URL=http://localhost:3000

# .env.production
VITE_API_URL=https://api.prod.example.com
```

### 3. 应用环境变量

在项目中，你可以通过 `import.meta.env` 访问这些环境变量。

```javascript
// 在 Vue 组件或任何 JS 文件中
console.log(import.meta.env.VITE_API_URL);
```

### 4. 运行和构建项目

使用不同的环境变量运行和构建项目。在命令行中，你可以通过 `--mode` 选项指定环境。

```bash
# 开发环境
vite --mode development

# 生产环境
vite build --mode production
```

或者，你可以在 `package.json` 的脚本中定义不同的命令，以便更方便地运行和构建。

```json
// package.json
{
  "scripts": {
    "dev": "vite --mode development",
    "build:dev": "vite build --mode development",
    "build:prod": "vite build --mode production"
  }
}
```

然后，你可以使用 npm 或 yarn 运行这些脚本：

```bash
npm run dev
npm run build:dev
npm run build:prod
```

或者，如果你使用 Yarn：

```bash
yarn dev
yarn build:dev
yarn build:prod
```

通过这种方式，你可以轻松地在不同的环境之间切换，并针对不同的环境进行开发和打包。记得在部署到生产环境之前，使用生产环境的配置进行构建，以确保所有的环境变量和优化都已正确设置。