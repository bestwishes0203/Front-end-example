// src/store/useUserStore.js
import { defineStore } from 'pinia';

export const useUserStore = defineStore('user', {
    state: () => ({
        username: 'Guest',
        email: '',
    }),
    actions: {
        setUsername(newUsername) {
            this.username = newUsername;
        },
        setEmail(newEmail) {
            this.email = newEmail;
        },
    },
});