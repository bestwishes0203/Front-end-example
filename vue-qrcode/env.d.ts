/// <reference types="vite/client" />
declare module 'qrcodejs2';
declare module '*.vue' {
    import { ComponentOptions } from 'vue'
    const componentOptions: ComponentOptions
    export default componentOptions
}