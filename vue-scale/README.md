## 手写scale方法

```js
import {ref} from "vue";

export default function windowResize() {
    // * 指向最外层容器
    const screenRef = ref();
    // * 定时函数
    const timer = ref(0);
    // * 默认缩放值
    const scale = {
        width: "1",
        height: "1",
    };

    // * 设计稿尺寸（px）
    const baseWidth = 1920;
    const baseHeight = 1080;

    // * 需保持的比例（默认1.77778）
    const baseProportion = parseFloat((baseWidth / baseHeight).toFixed(5));
    const calcRate = () => {
        // 当前宽高比
        const currentRate = parseFloat(
            (window.innerWidth / window.innerHeight).toFixed(5)
        );
        if (screenRef.value) {
            if (currentRate > baseProportion) {
                // 表示更宽
                scale.width = (
                    (window.innerHeight * baseProportion) /
                    baseWidth
                ).toFixed(5);
                scale.height = (window.innerHeight / baseHeight).toFixed(5);
                screenRef.value.style.transform = `scale(${scale.width}, ${scale.height})`;
            } else {
                // 表示更高
                scale.height = (
                    window.innerWidth /
                    baseProportion /
                    baseHeight
                ).toFixed(5);
                scale.width = (window.innerWidth / baseWidth).toFixed(5);
                screenRef.value.style.transform = `scale(${scale.width}, ${scale.height})`;
            }
        }
    };

    const resize = () => {
        clearTimeout(timer.value);
        timer.value = window.setTimeout(() => {
            calcRate();
        }, 200);
    };

    // 改变窗口大小重新绘制
    const windowDraw = () => {
        window.addEventListener("resize", resize);
    };

    // 改变窗口大小重新绘制
    const unWindowDraw = () => {
        window.removeEventListener("resize", resize);
    };

    return {
        screenRef,
        calcRate,
        windowDraw,
        unWindowDraw,
    };
}
```

```html

<template>
    <div ref="screenRef">
        <div class="flex-center">
            <form-view></form-view>
        </div>
    </div>
</template>

<script setup lang="ts">
    import FormView from './components/FormView.vue';
    import windowResize from '@/utils/resize';  // 引入resize文件
    import {onMounted, onUnmounted} from 'vue';

    const {screenRef, calcRate, windowDraw, unWindowDraw} = windowResize()

    onMounted(() => {
        // 监听浏览器窗口尺寸变化
        windowDraw()
        calcRate()
    })

    onUnmounted(() => {
        unWindowDraw();
    })
</script>

<style>
    .flex-center {
        width: 100%;
        height: 100vh;
        background-color: gray;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    body,
    html {
        width: 100vw;
        height: 100vh;
        overflow: scroll;
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    img {
        width: 100%;
        height: 100%;
    }

    /* 针对Webkit核心的浏览器（如Chrome和Safari） */
    ::-webkit-scrollbar {
        width: 0;
        height: 0;
    }

    ::-webkit-scrollbar-thumb {
        background: #c0c0c0;
        /* 滚动条滑块的背景颜色 */
        border-radius: 6px;
        /* 滑块的圆角 */
    }

    ::-webkit-scrollbar-track {
        background: #f1f1f1;
        /* 滚动条轨道的背景颜色 */
    }

    scrollbar {
        -moz-appearance: none;
        width: 12px;
        height: 12px;
    }

    scrollbar-thumb {
        background: #c0c0c0;
        border-radius: 6px;
    }

    scrollbar-track {
        background: #f1f1f1;
    }
</style>

```

## 使用scale-box

在Vue 3中使用`vue3-scale-box`进行屏幕适配，结合Composition API，可以按照以下步骤进行：

1. **安装`vue3-scale-box`**：
   在你的Vue 3项目中，首先需要安装`vue3-scale-box`。可以通过npm或yarn进行安装：
   ```shell
   npm install vue3-scale-box
   # 或者
   yarn add vue3-scale-box
   ```

2. **在组件中引入`vue3-scale-box`**：
   在你的Vue组件中引入`vue3-scale-box`：
   ```javascript
   <script setup>
   import ScaleBox from 'vue3-scale-box';
   </script>
   ```

3. **在模板中使用`ScaleBox`组件**：
   在Vue模板中，使用`ScaleBox`组件并设置相应的属性，如`width`、`height`、`bgc`和`delay`：

   ```html
   
   <template>
       <ScaleBox :width="1920" :height="1080" bgc="transparent" :delay="0" :isFlat="false">
           <div class="flex-center">
               <form-view></form-view>
           </div>
       </ScaleBox>
   </template>
   
   <script setup lang="ts">
       import ScaleBox from "vue3-scale-box";
       import FormView from './components/FormView.vue';
   </script>
   
   <style scoped>
       .flex-center {
           width: 1920px;
           height: 100vh;
           background-color: gray;
           display: flex;
           align-items: center;
           justify-content: center;
       }
   </style>
   ```



