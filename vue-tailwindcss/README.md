``` shell
npm install -D tailwindcss postcss autoprefixer

npx tailwindcss init -p
```


```ts
// tailwind.config.cjs
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
```



```css
//style.css
@tailwind base;
@tailwind components;
@tailwind utilities;
```



