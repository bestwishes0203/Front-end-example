/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        hfh: '#a31515', // 自定义一个主题颜色，我这里叫guozhaoxi
      },
    },
  },
  plugins: [],
}

