```shell

npm i xlsx jszip file-saver

```


```html
<template>
  <div>
    <button @click="exportToExcel">Export to Excel</button>
    <input type="file" @change="handleFileUpload">
  </div>
</template>

<script setup>
import { ref } from 'vue';
import * as XLSX from 'xlsx';

// 准备要导出的数据
const data = ref([
  { name: 'John', age: 30, city: 'New York' },
  { name: 'Mike', age: 25, city: 'Chicago' },
  { name: 'Sara', age: 28, city: 'Los Angeles' }
]);

const exportToExcel = () => {
  // 将数据转换为工作表
  const ws = XLSX.utils.json_to_sheet(data.value);
  // 创建工作簿并添加工作表
  const wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

  // 导出Excel文件
  XLSX.writeFile(wb, "data.xlsx");
};

const handleFileUpload = (event) => {
  const file = event.target.files[0];
  const reader = new FileReader();
  reader.onload = (e) => {
    const data = e.target.result;
    const workbook = XLSX.read(data, { type: 'binary' });
    const sheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[sheetName];
    const json = XLSX.utils.sheet_to_json(worksheet);
    console.log(json); // 处理或使用数据
  };
  reader.readAsBinaryString(file);
};
</script>

```



```html
<template>
    <div>
        <button @click="downloadZip">Download ZIP</button>
    </div>
</template>

<script setup>
    import { useZip } from './useZip';

    const { createAndDownloadZip } = useZip();

    const downloadZip = async () => {
        // 准备文件数组
        const files = [
            {
                name: 'hello.txt',
                content: 'Hello World\n'
            },
            {
                name: 'image.png',
                // 假设你有一个图片的Blob对象
                content: new Blob(['<img src="image-source" alt="Image">'], { type: 'image/png' }),
            }
        ];

        // 调用方法创建并下载ZIP文件
        await createAndDownloadZip(files);
    };
</script>

</script>

```



```js
// useZip.js
import JSZip from 'jszip';
import { saveAs } from 'file-saver';

export function useZip() {
  // 创建并下载ZIP文件的方法
  async function createAndDownloadZip(files, zipName = 'archive.zip') {
    const zip = new JSZip();
    
    // 将文件添加到ZIP中
    for (const file of files) {
      if (file.content instanceof Blob) {
        zip.file(file.name, file.content, { binary: true });
      } else {
        zip.file(file.name, file.content);
      }
    }

    // 生成ZIP文件并下载
    const content = await zip.generateAsync({ type: 'blob' });
    saveAs(content, zipName);
  }

  return { createAndDownloadZip };
}
```